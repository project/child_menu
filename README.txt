********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Child Menu Module
Author: Robert Castelo
Drupal: 7.x
********************************************************************
DESCRIPTION:

When a node is created as a child of another node, using Inline
Entity Form, this module automaticaly adds the node to the Main Menu
as a child of the parent node.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire module directory into your Drupal directory:
   sites/all/modules/


2. Enable the module by navigating to:

   administer > modules

  Click the 'Save configuration' button at the bottom to commit your
  changes.



********************************************************************
CONFIGURATION:

None




********************************************************************
AUTHOR CONTACT

- Commission New Features:
   http://drupal.org/user/3555/contact



********************************************************************
ACKNOWLEDGEMENT



